{
  description = "my nix doom-emacs configuration";

  inputs = {
    # doom-emacs = {
    #   url = "github:doomemacs/doomemacs/master";
    #   flake = false;
    # };
    nix-doom-emacs = {
      url = "github:nix-community/nix-doom-emacs";
      # inputs.doom-emacs.follows = "doom-emacs";
    };

  };


  outputs = { self, nixpkgs, nix-doom-emacs, ... }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.callPackage nix-doom-emacs {
      doomPrivateDir = ./doom.d;
      # extraPackages = epkgs: (with epkgs.melpaPackages; [
      #   request
      # ]);
    };
  };
}
